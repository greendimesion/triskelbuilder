package triskelbuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import termstore.TermStore;
import triple.Triple;
import triplestore.TripleStore;
import triskelbuilder.events.*;

public class Mounter {

    public TripleStore tripleStore;
    private TermStore termStore;

    public Mounter(TripleStore tripleStore, TermStore termStore) throws IOException {
        this.tripleStore = tripleStore;
        this.termStore = termStore;
    }

    public Triple translate(DataEvents event) {
        long idSubject = (long)termStore.translateSubject(event.getSubject());
        int idPredicate = termStore.translatePredicate(event.getPredicate());
        long idObject = (long) termStore.translateObject(event.getObject());
        return new Triple(idSubject, idPredicate, idObject);
    }

    public void mount() {
        try {
            File file = new File("Events.dat");
            ObjectInputStream objectInputStream;
            objectInputStream = new ObjectInputStream(new FileInputStream(file));         
            DataEvents dataEvent = (DataEvents) objectInputStream.readObject();

            while (dataEvent.getAction().getActionValue() != 'F') {
                AddSPOtoTermStore(dataEvent);
                Triple triple = translate(dataEvent);
                if (dataEvent.getAction().getActionValue() == ('A')) 
                    tripleStore.assertTriple(triple);
                else tripleStore.retractTriple(triple);
                
                dataEvent = (DataEvents) objectInputStream.readObject();
            }
            
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Mounter.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Exception = " + ex);
        }
    }

    private void AddSPOtoTermStore(DataEvents dataEvent) {
        termStore.addSubject(dataEvent.getSubject());
        termStore.addPredicate(dataEvent.getPredicate());
        termStore.addObject(dataEvent.getObject());
    }
}
