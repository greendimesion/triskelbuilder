package triskelbuilder;

import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreSerializer;
import java.io.IOException;
import java.util.Date;
import termstore.TermStore;
import termstore.TermStoreSerialize;
import termstore.avltree.AvlTermStore;
import termstore.avltree.AvlTermStoreSerialize;
import triplestore.TripleStore;
import triplestore.TripleStoreSerializer;

public class TriskelBuilderController {
  
    public static void main(String[] args) throws IOException {
        TripleStore tripleStore = new CubeTripleStore();
        TripleStoreSerializer tripleStoreSerializer = new CubeTripleStoreSerializer(tripleStore);
        TermStore termStore = new AvlTermStore();
        TermStoreSerialize termStoreSerialize = new AvlTermStoreSerialize((AvlTermStore) termStore);
        Mounter mounter = new Mounter(tripleStore, termStore);
        mounter.mount();
        SnapShotBuilder snapShotBuilder = new SnapShotBuilder(tripleStoreSerializer, termStoreSerialize);
        Date date = new Date();
        snapShotBuilder.execute(Long.toString(date.getTime()));
    }
}
