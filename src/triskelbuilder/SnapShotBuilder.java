package triskelbuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import termstore.TermStoreSerialize;
import triplestore.TripleStoreSerializer;

public class SnapShotBuilder {
    
    TripleStoreSerializer tripleStoreSerializer;
    TermStoreSerialize termStoreSerialize; 

    public SnapShotBuilder(TripleStoreSerializer tripleStoreSerializer, TermStoreSerialize termStoreSerialize) {
        this.tripleStoreSerializer = tripleStoreSerializer;
        this.termStoreSerialize = termStoreSerialize;
    }
    
    public void execute(String transaction) throws IOException{
        tripleStoreSerialize(transaction);
        termStoreSerialize(transaction);
    }        
        

    private void termStoreSerialize(String transaction) throws IOException {
        File termStorefile = new File("TriskeldbTermStore-"+transaction); 
        if (!termStorefile.exists()) termStorefile.createNewFile();
        try (ObjectOutputStream termStoreOutputStream = new ObjectOutputStream(new FileOutputStream(termStorefile))) {
            termStoreSerialize.execute(termStoreOutputStream);
        }
    }

    private void tripleStoreSerialize(String transaction) throws IOException {
        File tripleStorefile = new File("TriskeldbTripleStore-"+transaction);
        if (!tripleStorefile.exists()) tripleStorefile.createNewFile();
        try (ObjectOutputStream tripleStoreOutputStream = new ObjectOutputStream(new FileOutputStream(tripleStorefile))) {
            tripleStoreSerializer.execute(tripleStoreOutputStream);
        }
    }
}
