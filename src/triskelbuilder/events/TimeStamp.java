package triskelbuilder.events;

import java.io.Serializable;

public class TimeStamp implements Serializable {

    private static double systemNumber = 0;

    public TimeStamp() {
        TimeStamp.systemNumber++;
    }

    public double getSystemNumber() {
        return systemNumber;
    }
}
