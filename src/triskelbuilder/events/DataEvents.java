package triskelbuilder.events;

import action.Action;
import java.io.Serializable;

public class DataEvents extends Events implements Serializable {

    private StringTriple triplet;
    private Action action;
    private TimeStamp timeStamp;

    public DataEvents(Long subject, Integer predicate, Long object, char action) {
        //triplet = new Triple(subject, predicate, object);
        this.action = new Action(action);
        timeStamp = new TimeStamp();
    }

    public DataEvents(String subject, String predicate, String object, char action) {
        triplet = new StringTriple(subject, predicate, object);
        this.action = new Action(action);
        timeStamp = new TimeStamp();
    }

    public StringTriple getTriplet() {
        return triplet;
    }

    public String getSubject() {
        return triplet.getSubject();
    }

    public String getPredicate() {
        return triplet.getPredicate();
    }

    public String getObject() {
        return triplet.getObject();
    }

    public Action getAction() {
        return action;
    }

    public TimeStamp getTimeStamp() {
        return timeStamp;
    }
    /*
     public String[] getEventValue() {
     Object[] eventDatas = {
     triplet.getSubject(),
     triplet.getPredicate(),
     triplet.getObject(),
     String.valueOf(action.getActionValue()),
     String.valueOf(timeStamp.getSystemNumber())
     };
     return eventDatas;
     }*/

    private class StringTriple implements Serializable {

        private String subject;
        private String predicate;
        private String object;

        public StringTriple(String subject, String predicate, String object) {
            this.subject = subject;
            this.predicate = predicate;
            this.object = object;
        }

        public String getSubject() {
            return subject;
        }

        public String getPredicate() {
            return predicate;
        }

        public String getObject() {
            return object;
        }
    }
}
