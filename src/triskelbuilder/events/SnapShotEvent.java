package triskelbuilder.events;

public class SnapShotEvent extends Events {

    private String name;

    public SnapShotEvent() {
        this.name = "#1#";
    }

    public String getName() {
        return name;
    }
}
