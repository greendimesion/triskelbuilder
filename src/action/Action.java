package action;

import java.io.Serializable;

public class Action implements Serializable {

    private char action;

    public Action(char action) {
        this.action = action;
    }

    public char getActionValue() {
        return action;
    }
}
