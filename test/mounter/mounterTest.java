package mounter;

import cubetriplestore.CubeTripleStore;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.Before;
import termstore.TermStore;
import termstore.avltree.AvlTermStore;
import triple.Triple;
import triplestore.TripleStore;
import triskelbuilder.Mounter;
import triskelbuilder.events.DataEvents;

public class mounterTest {

    private DataEvents dataEvent;
    private ObjectOutputStream objectOutputStream;

    @Before
    public void createEventsFile() throws Exception {
        File file = new File("Events.dat");
        if (!file.exists()) {
            file.createNewFile();
        }
        objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        dataEvent = new DataEvents("Juan", "tiene", "perro", 'A');
        objectOutputStream.writeObject(dataEvent);
        dataEvent = new DataEvents("perro", "tiene", "pulga", 'A');
        objectOutputStream.writeObject(dataEvent);   
        dataEvent = new DataEvents("Tobi", "es", "perro", 'A');
        objectOutputStream.writeObject(dataEvent);
        dataEvent = new DataEvents("Juan", "tiene", "perro", 'E');
        objectOutputStream.writeObject(dataEvent);
        dataEvent = new DataEvents("perro", "tiene", "rabo", 'A');
        objectOutputStream.writeObject(dataEvent);
        dataEvent = new DataEvents("perro", "tiene", "pulga", 'E');
        objectOutputStream.writeObject(dataEvent);
        dataEvent = new DataEvents("perro", "tiene", "pulga", 'F');
        objectOutputStream.writeObject(dataEvent);
        objectOutputStream.close();
    }

    @Test
    public void check() throws IOException, ClassNotFoundException {
        TripleStore tripleStore = new CubeTripleStore();
        TermStore termStore = new AvlTermStore();
        Mounter mounter = new Mounter(tripleStore,termStore);
        mounter.mount();
        //Comprueba que existe la tripleta perro tiene pulga
        Assert.assertTrue(mounter.tripleStore.checkTriple(new Triple(1, 0, 1)));
        //Comprueba que no existe la tripleta eliminada juan tiene perro
        Assert.assertFalse(mounter.tripleStore.checkTriple(new Triple(0, 0, 0)));
    }
}